using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Contacts.Models
{

    public class ContactTagViewModel
    {
        public List<Contact> Contacts {get;set;}
        public SelectList Tags {get;set;}
        public string ContactTag {get;set;}
        public string SearchString {get;set;}


    }

}