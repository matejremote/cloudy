using System;
using System.ComponentModel.DataAnnotations;

namespace Contacts.Models
{
    public class Contact
    {

        public int Id { get; set; }
        public string Name { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        public string Tag { get; set; }


    }
}