using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Contacts.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ContactsContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ContactsContext>>()))
            {
                if (context.Contact.Any())
                {
                    return;   // DB has been seeded
                }

                context.Contact.AddRange(
                    new Contact
                    {
                        Name = "Matej",
                        Phone = "+385915439060",
                        Tag = "Me"

                    },

                    new Contact
                    {
                        Name = "A1 Telekom",
                        Phone = "+385 91 7125 091",
                        Tag = "Foe"
                    },

                    new Contact
                    {
                        Name = "Ana",
                        Phone = "+385 92 3310 112",
                        Tag = "Friend"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}